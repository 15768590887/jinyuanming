package com.github.wxiaoqi.security.jinyuanming.rest;

import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.jinyuanming.biz.CustomerRegcodeBiz;
import com.github.wxiaoqi.security.jinyuanming.entity.CustomerRegcode;
import com.github.wxiaoqi.security.jinyuanming.utils.RegCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("jinyuanming/customerRegcode")
public class CustomerRegcodeController extends BaseController<CustomerRegcodeBiz,CustomerRegcode> {

    @Autowired
    private CustomerRegcodeBiz customerRegcodeBiz;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<CustomerRegcode> add(@RequestBody CustomerRegcode entity) {
        entity.setStatus(1);
        entity.setRegcode(RegCodeUtil.generateShortUuid());
        customerRegcodeBiz.insertSelective(entity);
        return new ObjectRestResponse<CustomerRegcode>();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<CustomerRegcode> get(@PathVariable String id) {
        ObjectRestResponse<CustomerRegcode> entityObjectRestResponse = new ObjectRestResponse<>();
        CustomerRegcode o = customerRegcodeBiz.selectById(id);
        entityObjectRestResponse.data(o);
        return entityObjectRestResponse;
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ObjectRestResponse<CustomerRegcode> update(@RequestBody CustomerRegcode entity) {
        customerRegcodeBiz.updateSelectiveById(entity);
        return new ObjectRestResponse<CustomerRegcode>();
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<CustomerRegcode> remove(@PathVariable String id) {
        customerRegcodeBiz.deleteById(id);
        return new ObjectRestResponse<CustomerRegcode>();
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<CustomerRegcode> list(@RequestParam Map<String, Object> params) {
        // 查询列表数据
        Query query = new Query(params);
        TableResultResponse<CustomerRegcode> list=customerRegcodeBiz.selectByQuery(query);
        return list;
    }

}