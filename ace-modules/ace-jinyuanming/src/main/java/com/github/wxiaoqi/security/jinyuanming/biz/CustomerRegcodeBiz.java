package com.github.wxiaoqi.security.jinyuanming.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.jinyuanming.entity.CustomerRegcode;
import com.github.wxiaoqi.security.jinyuanming.mapper.CustomerRegcodeMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 
 *
 * @author ckm
 * @email 
 * @date 2020-06-05 14:20:58
 */
@Service
public class CustomerRegcodeBiz extends BaseBiz<CustomerRegcodeMapper,CustomerRegcode> {

    @Autowired
    CustomerRegcodeMapper customerRegcodeMapper;


    public CustomerRegcode findByRegcode(String regcode, Integer status){
        return customerRegcodeMapper.findByRegcode(regcode,status);
    }

}