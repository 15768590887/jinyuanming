package com.github.wxiaoqi.security.jinyuanming.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author ckm
 * @email 
 * @date 2020-06-05 14:20:58
 */
@Table(name = "jinyuanming_customer_regcode")
public class CustomerRegcode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private String id;
	
	    //
    @Column(name = "regcode")
    private String regcode;
	
	    //
    @Column(name = "status")
    private Integer status;
	
	    //
    @Column(name = "created_by")
    private String createdBy;
	
	    //
    @Column(name = "created_time")
    private Date createdTime;
	
	    //
    @Column(name = "updated_by")
    private String updatedBy;
	
	    //
    @Column(name = "updated_time")
    private Date updatedTime;
	
	    //
    @Column(name = "comp_id")
    private String compId;
	

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setRegcode(String regcode) {
		this.regcode = regcode;
	}
	/**
	 * 获取：
	 */
	public String getRegcode() {
		return regcode;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * 获取：
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * 设置：
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * 获取：
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * 设置：
	 */
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdatedTime() {
		return updatedTime;
	}
	/**
	 * 设置：
	 */
	public void setCompId(String compId) {
		this.compId = compId;
	}
	/**
	 * 获取：
	 */
	public String getCompId() {
		return compId;
	}
}
