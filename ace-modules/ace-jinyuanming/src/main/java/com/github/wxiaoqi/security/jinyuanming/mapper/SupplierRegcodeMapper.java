package com.github.wxiaoqi.security.jinyuanming.mapper;

import com.github.wxiaoqi.security.jinyuanming.entity.SupplierRegcode;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author ckm
 * @email 
 * @date 2020-06-05 14:20:58
 */

public interface SupplierRegcodeMapper extends Mapper<SupplierRegcode> {

    @Select(" select s.* from jinyuanming_supplier_regcode s  where s.regcode = #{regcode} and  s.status = #{status}")
    public SupplierRegcode findByRegcode(@Param("regcode") String regcode, @Param("status") Integer status);

}