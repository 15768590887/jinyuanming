package com.github.wxiaoqi.security.jinyuanming.biz;


import com.github.wxiaoqi.security.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import com.github.wxiaoqi.security.admin.utils.PassWord;
import com.github.wxiaoqi.security.common.constant.UserConstant;
import com.github.wxiaoqi.security.common.exception.auth.UserInvalidException;
import com.github.wxiaoqi.security.jinyuanming.entity.RegUser;
import com.github.wxiaoqi.security.jinyuanming.mapper.RegUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegUserBiz extends BaseBiz<RegUserMapper, RegUser> {

    @Autowired
    RegUserMapper regUserMapper;
    @Autowired
    private PassWord passWordUtil;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);


    @Transactional
    public int reg(RegUser entity) {
        String passWord=passWordUtil.decipheringByRSA(entity.getPassword());
        entity.setPassword( new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(passWord));
        RegUser temp=new RegUser();
        temp.setUsername(entity.getUsername());
        temp= regUserMapper.selectOne(temp);
        if(temp!=null)
            throw new UserInvalidException("该手机号已被使用");

        //公司
        entity.setCompId("fedc92219a234484864ba78a5ff99bbc");
        //权限
        int res=super.insertSelective(entity);

       // int res=regUserMapper.insert(entity);


        /*
        Depart depart=departMapper.selectByPrimaryKey(entity.getDepartId());
        if(depart!=null && entity.getCompId()==null)
            entity.setCompId(depart.getCompId());

        int res=super.insertSelective(entity);
        if(StringUtil.isNotEmpty(entity.getGroupIds()))
        {
            String[] groupIds=entity.getGroupIds().split("\\|");

            for(String groupId : groupIds)
            {

                groupMapper.insertGroupMembersById(groupId,entity.getId());

            }
        }
        */
        return res;
    }


}
