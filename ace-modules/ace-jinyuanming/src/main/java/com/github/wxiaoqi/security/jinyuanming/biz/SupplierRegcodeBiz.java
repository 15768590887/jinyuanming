package com.github.wxiaoqi.security.jinyuanming.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.jinyuanming.entity.SupplierRegcode;
import com.github.wxiaoqi.security.jinyuanming.mapper.SupplierRegcodeMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 
 *
 * @author ckm
 * @email 
 * @date 2020-06-05 14:20:58
 */
@Service
public class SupplierRegcodeBiz extends BaseBiz<SupplierRegcodeMapper,SupplierRegcode> {


    @Autowired
    SupplierRegcodeMapper supplierRegcodeMapper;


    public SupplierRegcode findByRegcode(String regcode,Integer status){
        return supplierRegcodeMapper.findByRegcode(regcode,status);
    }


}