package com.github.wxiaoqi.security.jinyuanming.rest;

import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.jinyuanming.biz.SupplierRegcodeBiz;
import com.github.wxiaoqi.security.jinyuanming.entity.SupplierRegcode;
import com.github.wxiaoqi.security.jinyuanming.utils.RegCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@Controller
@RequestMapping("jinyuanming/supplierRegcode")
public class SupplierRegcodeController extends BaseController<SupplierRegcodeBiz,SupplierRegcode> {

    @Autowired
    private SupplierRegcodeBiz supplierRegcodeBiz;


    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<SupplierRegcode> add(@RequestBody SupplierRegcode entity) {
        entity.setStatus(1);
        entity.setRegcode(RegCodeUtil.generateShortUuid());
        supplierRegcodeBiz.insertSelective(entity);
        return new ObjectRestResponse<SupplierRegcode>();
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<SupplierRegcode> get(@PathVariable String id) {
        ObjectRestResponse<SupplierRegcode> entityObjectRestResponse = new ObjectRestResponse<>();
        SupplierRegcode o = supplierRegcodeBiz.selectById(id);
        entityObjectRestResponse.data(o);
        return entityObjectRestResponse;
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ObjectRestResponse<SupplierRegcode> update(@RequestBody SupplierRegcode entity) {
        supplierRegcodeBiz.updateSelectiveById(entity);
        return new ObjectRestResponse<SupplierRegcode>();
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<SupplierRegcode> remove(@PathVariable String id) {
        supplierRegcodeBiz.deleteById(id);
        return new ObjectRestResponse<SupplierRegcode>();
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<SupplierRegcode> list(@RequestParam Map<String, Object> params) {
        // 查询列表数据
        Query query = new Query(params);
        TableResultResponse<SupplierRegcode> list=supplierRegcodeBiz.selectByQuery(query);
        return list;
    }
}