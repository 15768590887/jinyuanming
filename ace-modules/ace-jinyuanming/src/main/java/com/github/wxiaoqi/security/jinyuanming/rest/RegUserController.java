package com.github.wxiaoqi.security.jinyuanming.rest;
import com.github.wxiaoqi.security.jinyuanming.biz.CustomerRegcodeBiz;
import com.github.wxiaoqi.security.jinyuanming.biz.RegUserBiz;
import com.github.wxiaoqi.security.jinyuanming.biz.SupplierRegcodeBiz;
import com.github.wxiaoqi.security.jinyuanming.entity.CustomerRegcode;
import com.github.wxiaoqi.security.jinyuanming.entity.RegUser;
import com.github.wxiaoqi.security.jinyuanming.entity.SupplierRegcode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("jinyuanming/reguser")
public class RegUserController {

    @Autowired
    private RegUserBiz regUserBiz;

    @Autowired
    private SupplierRegcodeBiz supplierRegcodeBiz;
    @Autowired
    private CustomerRegcodeBiz customerRegcodeBiz;


    //注册
    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    @ResponseBody
    public int register(@RequestBody @Validated RegUser entity) {
        //判断注册码是否存在且有效
        String regtype=entity.getRegtype();
        System.out.println("==============================================");
        System.out.println(regtype);
        //注册码
        String regcode=entity.getRegcode();

        if(regtype.equals("供应商")){
            SupplierRegcode supplierRegcode= supplierRegcodeBiz.findByRegcode(regcode,1);
            if(supplierRegcode!=null){
                regUserBiz.reg(entity);
                //更改注册码失效
                supplierRegcode.setStatus(0);
                supplierRegcodeBiz.updateSelectiveById(supplierRegcode);
            }else{
                return 2;
            }
        }else if(regtype.equals("客户")){
            CustomerRegcode customerRegcode= customerRegcodeBiz.findByRegcode(regcode,1);
            if(customerRegcode!=null){
                regUserBiz.reg(entity);
                customerRegcode.setStatus(0);
                customerRegcodeBiz.updateSelectiveById(customerRegcode);
            }else{
                return 3;
            }
        }
        return 1;
    }

}
