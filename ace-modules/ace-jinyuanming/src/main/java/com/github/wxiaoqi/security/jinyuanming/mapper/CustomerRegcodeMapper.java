package com.github.wxiaoqi.security.jinyuanming.mapper;

import com.github.wxiaoqi.security.jinyuanming.entity.CustomerRegcode;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author ckm
 * @email 
 * @date 2020-06-05 14:20:58
 */

public interface CustomerRegcodeMapper extends Mapper<CustomerRegcode> {

    @Select(" select c.* from jinyuanming_customer_regcode c  where c.regcode = #{regcode} and  c.status = #{status} ")
    public CustomerRegcode findByRegcode(@Param("regcode") String regcode, @Param("status") Integer status);
}
